from gmail import gmail, getLabelUkoly, readUnreadMail
from sheet import sheet, sheetReadUnreadMail

puvodniverze = {}


def check_sync():
    gmailMails = gmail.users().messages().list(userId='me', labelIds=[getLabelUkoly()]).execute()
    unreadGmailMails = gmail.users().messages().list(userId='me', labelIds=[getLabelUkoly(), "UNREAD"]).execute()
    sheetMails = sheet.batch_get(["F2:G999"])

    gmailMails = [mail["id"] for mail in gmailMails["messages"]]
    unreadGmailMails = [mail["id"] for mail in unreadGmailMails["messages"]]

    for mail in sheetMails[0]:
        if len(mail) == 2 and mail[1] in gmailMails:
            mailId = mail[1]
            isUnreadGmail = mailId in unreadGmailMails
            isUnreadSheet = mail[0] == "FALSE"
            if mailId not in puvodniverze.keys():
                puvodniverze[mailId] = isUnreadGmail
            isOriginalUnread = puvodniverze[mailId]

            if isOriginalUnread != isUnreadGmail or isOriginalUnread != isUnreadSheet:
                puvodniverze[mailId] = not isOriginalUnread
                readUnreadMail(mailId, isOriginalUnread)
                sheetReadUnreadMail(mailId, isOriginalUnread)

    pass
