import re

class ParsedMail:
    jmeno: str
    trida: str
    ukol: str
    isHomework: bool


def parse_mail(text: str, subject: str) -> ParsedMail:
    text = text.replace("\r\n", "\n")
    parsed = ParsedMail()
    lastLine = [line for line in text.split("\n") if line][-1]
    jmeno = lastLine

    new_trida = ""
    trida = re.search('[0-9].[A-Za-z]', lastLine)
    if trida:
        new_trida = trida.string[trida.regs[-1][0]:trida.regs[-1][1]]
        jmeno = jmeno.replace(new_trida, '')
        # print('Trida: ', new_trida)
    # else:
        # print('Not in Trida')

    jmeno = jmeno.replace(',', '').replace('.', '')

    # print('Jmeno: ', jmeno)

    isHomework = False
    for sub in subject.lower().split(" "):
        if "dcvx" in sub:
            isHomework = True


    parsed.jmeno = jmeno
    parsed.trida = new_trida
    parsed.ukol = subject
    parsed.isHomework = isHomework
    return parsed


if __name__ == '__main__':
    retezec = """Dobrý den, blblblblaaaablabbkal bknknksasbkbkajsbjbkf, blnkbknklfnljl, bjblh  kklslk 
 jkjljkslkdksjksjljka kjksjdla jjksjdkksd
fdkjhfkdsnkf
kdfjjdhfksdh fdjfhjsdhff dfjkjdhlahfj
Ester Halířová, 6.c"""
    mail = parse_mail(retezec, "")
    print("parsed mail: ", mail.jmeno, mail.trida)
