
def save_file(filename, text):
    with open(filename, "w+") as f:
        f.write(text)

def load_file(filename) -> str:
    try:
        with open(filename, "r") as f:
            return f.read()
    except:
        pass