import time

from gmail import process_mails
from sync import check_sync
from util import load_file, save_file


def main():
    # timestamp is 0 or the one stored in timestamp.txt
    timestamp = 0
    fileStamp = load_file("timestamp.txt")
    if fileStamp:
        timestamp = int(fileStamp)

    # Check for new mails every 10 seconds
    while 1:
        print("GETTINGS MAILS")
        try:
            timestamp = process_mails(timestamp)
            save_file("timestamp.txt", str(timestamp))
        except Exception as e:
            pass

        try:
            check_sync()
        except Exception as e:
            pass
        print("SLEEPING")
        time.sleep(1)


if __name__ == '__main__':
    main()
