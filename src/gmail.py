import base64
import os
import pickle
import urllib

import google_auth_oauthlib.flow
from googleapiclient.discovery import build

from parsing import parse_mail
from sheet import add_sheet_row

SCOPES = 'https://mail.google.com/'


def urldecode(text):
    return urllib.parse.unquote(text)


def authorize():
    creds = None
    if os.path.exists("token.pickle"):
        with open("token.pickle", "rb") as f:
            creds = pickle.load(f)

    # store = file.Storage('token.json')
    # creds = store.get()
    if not creds:
        flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file("client_id.json", SCOPES)
        creds = flow.run_console()

    with open("token.pickle", "wb+") as f:
        pickle.dump(creds, f)

    return creds


credentials = authorize()
gmail = build('gmail', 'v1', credentials=credentials)


def process_mails(timestamp):
    results = gmail.users().messages().list(userId='me', labelIds=['INBOX']).execute()
    messages = results.get('messages', [])
    print("Messages:")

    latest_timestamp = timestamp
    for message in messages:
        try:
            msg = gmail.users().messages().get(userId='me', id=message['id']).execute()
            payload = msg["payload"]
            ts = int(msg["internalDate"])
            if ts <= timestamp:
                continue
            elif ts > latest_timestamp:
                latest_timestamp = ts

            headers = {h["name"]: h["value"] for h in payload["headers"]}

            date = headers["Date"]
            # name = headers["From"]
            subject = headers["Subject"]

            body = ""
            attachments = []

            for part in payload["parts"]:
                if part["mimeType"] == "text/plain":
                    body = part["body"]["data"]
                elif part["mimeType"] == "multipart/alternative":
                    for subpart in part["parts"]:
                        if subpart["mimeType"] == "text/plain":
                            body = subpart["body"]["data"]
                elif part["mimeType"] == "text/html":
                    pass
                else:
                    attachments.append(part)

            text = base64.decodebytes(body.encode("ascii"))
            text = urldecode(text.decode("utf-8"))
            print(text)
            print(len(attachments))

            mail = parse_mail(text, subject)
            if mail.isHomework:
                add_sheet_row(
                    [mail.jmeno, "---", mail.ukol, date, f"https://mail.google.com/mail/u/0/#inbox/{msg['id']}", False,
                     msg["id"], " "])
                addRemoveMailLabel(msg["id"], [getLabelUkoly()], ["INBOX"])

        except Exception as e:
            print("Sakra něco zas:", e)
            pass

    return latest_timestamp


def addRemoveMailLabel(mailId: str, labelsToAdd: list, labelsToRemove: list):
    gmail.users().messages().modify(userId="me", id=mailId,
                                    body={"addLabelIds": labelsToAdd, "removeLabelIds": labelsToRemove}).execute()


def readUnreadMail(mailId: str, isRead: bool):
    if isRead:
        addRemoveMailLabel(mailId, [], ["UNREAD"])
    else:
        addRemoveMailLabel(mailId, ["UNREAD"], [])


LABEL = "ukoly"
labelId = ""


def getLabelUkoly():
    global labelId
    if not labelId:
        allLabels = gmail.users().labels().list(userId='me').execute()
        for label in allLabels["labels"]:
            if label["name"] == LABEL:
                labelId = label["id"]
                break
    return labelId


if __name__ == '__main__':
    process_mails(0)
