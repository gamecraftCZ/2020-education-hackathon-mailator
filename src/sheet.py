import gspread
from oauth2client.service_account import ServiceAccountCredentials

scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('HackathonVzdelavani.json', scope)
client = gspread.authorize(creds)

sheet = client.open('Students').sheet1

# Jméno, předmět, úkol, datum, odkaz mail, Přečteno, Odkaz na mail, MailId
def add_sheet_row(data: list):
    sheet.append_row(data)

    Students = sheet.get_all_records()
    print(Students)


def sheetReadUnreadMail(mailId: str, isRead: bool):
    a = sheet.find(mailId, None, 7)
    sheet.update_cell(a.row, 6, isRead)
    pass


# Vypise radek
if __name__ == '__main__':
    sheetReadUnreadMail("17413995a4ab22d8", True)
    Students = sheet.get_all_records()
    print(Students)
